import { BaseLayout } from "./layouts/BaseLayout";
import { Checkout } from "./pages/Checkout";
import { Contact } from "./pages/Contact";
import { Home } from "./pages/Home";
import { Login } from "./pages/Login";
import { Movies } from "./pages/Movies";
import { Register } from "./pages/Register";
import { Showtime } from "./pages/Showtime";
import {Routes, Route} from "react-router-dom";

export function Router(){
    return(

        <Routes>
            <Route path="/" element={<BaseLayout/>}>
                <Route path="/" element={<Home/>}/>
                <Route path="/movies" element={<Movies/>}/>
                <Route path="/showtime/:movieId" element={<Showtime/>}/>
                <Route path="/login" element={<Login />}/>
                <Route path="/register" element={<Register/>}/>
                <Route path="/contact" element={<Contact/>}/>
                <Route path="/checkout" element={<Checkout/>}/>
            </Route>
        </Routes>
    )
}