import { createContext, useEffect, useState } from "react"
import PropTypes from "prop-types"
export const ApplicationContext = createContext()
export function ApplicationContextProvider({children}){

    const [filmes, setFilmes] = useState([])
    useEffect(()=>{
        /*FAKE API */
        // const url = "http://localhost:3000/cinema"
        /*BACKEND C{IN}EMA*/
        const url = "http://localhost:3000/filmes"
        const config ={
            method:"GET"
        }
        fetch(url, config)
        .then(response => response.json())
            .then(response => {
                setFilmes(response)
            })
    }, [])
    return(
        <ApplicationContext.Provider value={{
            filmes, setFilmes
        }}>
            {children}
        </ApplicationContext.Provider>
    )
}

ApplicationContextProvider.propTypes = {
    children: PropTypes.node
}