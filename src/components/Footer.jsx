import styles from "./footer.module.css"
export function Footer(){
    return(
        <>
        <div className={styles.footer}>
            <div className={styles.content}>
                <div className={styles.info}>
                    <div>
                        <h2>Endereço</h2>
                        <p>Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa Viagem, Niterói - RJ
                        CEP: 24210-315 </p>
                    </div>
                    <div>
                        <h2>Fale conosco</h2>
                        <p>contato@injunior.com.br</p>
                    </div>
                    <div>
                           <a href="https://www.instagram.com/injunioruff/"> <img src="/src/assets/icone-instagram.svg"/></a>
                           <a href="https://www.facebook.com/injunioruff"> <img src="/src/assets/icone-facebook.svg"/></a>
                           <a href="https://www.linkedin.com/company/in-junior/"><img src="/src/assets/icone-linkedin.svg"/></a>
                    </div>
                </div>
                <img className={styles.ticketIN} src="/src/assets/ingresso.svg"/>
                <iframe className={styles.map}src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.1901689114084!2d-43.13596662468965!3d-22.906355579255436!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817ed79f10f3%3A0xb39c7c0639fbc9e8!2sIN%20Junior%20-%20Empresa%20Junior%20de%20Computa%C3%A7%C3%A3o%20da%20UFF!5e0!3m2!1spt-BR!2sbr!4v1709683244324!5m2!1spt-BR!2sbr" width="325" height="255" style={{border:0}} allowFullScreen="" loading="lazy" referrerPolicy="no-referrer-when-downgrade"></iframe>
            </div>
            <div className={styles.copyright}>
                <p>
                © Copyright 2023. IN Junior. Todos os direitos reservados. Niterói, Brasil.
                </p>
            </div>
           </div>
        </>
    )
}