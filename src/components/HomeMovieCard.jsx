import styles from './homemoviecard.module.css'
export function HomeMovieCard({title, imgUrl}){
    if(imgUrl !== "" ){
        return(
            <div className={styles.homeMovieCard}>
                <img src={imgUrl}/>
                <h2>{title}</h2>
                <div className={styles.availableSessions}><p>SESSÕES DISPONÍVEIS</p></div>
                

            </div> 
        )   
    }else{
        return(
            <></>
        )
    }
    
}