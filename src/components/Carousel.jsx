import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import styles from "./carousel.module.css";

export function Carousel(){
    var settings = {
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
      };
    return(
        <div className={styles.sliderContainer}>
            <Slider  {...settings}>
                    <div className={styles.ticketP}>
                        <img src="/src/assets/ticketP.svg" />
                    </div>
                    <div style={{ width: 1440 }}>
                        <img src="/src/assets/ticketG.svg" />
                    </div>
                    <div className={styles.ticketDuo}>
                        <img src="/src/assets/ticketDuo.svg" />
                    </div>
        

            </Slider>
      </div>
    )
}