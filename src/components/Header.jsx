import { NavLink } from 'react-router-dom'
import styles from '/src/components/header.module.css'
export function Header(){
    return(
        
            <div className={styles.header}>
                <NavLink to="/"><img src='/src/assets/logo.svg'/></NavLink>
                
                <div className={styles.options}>
                    <NavLink to="/movies"><img src="/src/assets/icone-filmes.svg"/></NavLink>
                    <NavLink to="/login"><img src="/src/assets/icone-entrar.svg"/></NavLink>
                    <NavLink to="/contact"><img src="/src/assets/icone-ajuda.svg"/></NavLink>
                    
                </div>
            </div>
        
    )
}