import { NavLink } from 'react-router-dom'
import styles from './moviecard.module.css'
import PropTypes from 'prop-types'

export function MovieCard({id, title, imgUrl, sinopsys, genre, rating, director}){
    const basePath = "/src/assets/"
    var ratingUrl = basePath

    
    if(rating !==  null){
        if(rating === 0){
            ratingUrl += "livre.svg"
        }else
        if(rating === 1){
            ratingUrl += "10.svg"
        }else
        if(rating === 2){
            ratingUrl += "12.svg"
        }else 
        if(rating === 3){
            ratingUrl += "14.svg"
        }else
        if(rating === 4){
            ratingUrl += "16.svg"
        }else
        if(rating === 5){
            ratingUrl += "18.svg"
        }
    }
    
    
    

    if(imgUrl != ""){
        return(
            <div className={styles.movieCard}>
                {/* Container com info do card */}
                <div>
                    <img src={imgUrl}/>
                    <div className={styles.movieCardHeader}>
                        <h2>{title}</h2>
                        <img className={styles.rating} src={ratingUrl}/>
                    </div>
                    <p>{genre}</p>
                    <p>Direção: {director}</p>
                    <p>{sinopsys}</p>
                </div>
                <button className={styles.movieCardButton}>
                    <NavLink to={`/showtime/${id}`} style={{ textDecoration: 'none', color: 'inherit' }}>
                        Ver sessões
                    </NavLink>
                </button>
            </div>
            
        )
    }else{
        return (
            <></>
        )
    }
    
}

MovieCard.propTypes = {
    id: PropTypes.string,
    title:PropTypes.string,
    imgUrl:PropTypes.string,
    sinopsys:PropTypes.string,
    genre:PropTypes.string,
    rating:PropTypes.number,
    director:PropTypes.string,
}