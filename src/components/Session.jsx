import styles from './session.module.css'
export function Session({type, availableSessions}){
    var sessionName = "";
    if(type === 0 ){
        sessionName = "2D"
    }else if(type === 1){
        sessionName = "3D"
    }else if(type === 2){
        sessionName = "IMAX"
    }
    console.log(availableSessions)
    return(
        <div className={styles.container}>
            
            <h2 className={styles.sessionName}>{sessionName}</h2>
            <div className={styles.hourContainer}>
            {

                availableSessions.map(session=>{
                    return(
                        <div className={styles.hour}>
                            {session.horario}
                        </div>
                        
                    )
                })
                }
            </div>
            
            
        </div>
    )
}