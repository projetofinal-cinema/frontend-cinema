import { NavLink } from "react-router-dom";
import { ApplicationContext } from "../contexts/ApplicationContextProvider";
import { useContext, useEffect, useRef, useState } from "react";
import styles from "./home.module.css";
import { HomeMovieCard } from "../components/HomeMovieCard";
import { Carousel } from "../components/Carousel";



export function Home(){
    const {filmes, setFilmes} = useContext(ApplicationContext)
    const [randomFilme, setRandomFilme] = useState(null)
    const [displayedFilmes, setDisplayedFilmes] = useState([])

    useEffect(()=>{
        const sortedFilmes = [];
        const numOfFilmesToDisplay = 5;
        const usedIndexes = new Set();
        if(filmes.length > 0){
            if(filmes.length> numOfFilmesToDisplay){
                while(sortedFilmes.length < numOfFilmesToDisplay){
                    const randomIndex = Math.floor(Math.random() * filmes.length);
                    const selectedRandomFilme = filmes[randomIndex];
                    if(!usedIndexes.has(randomIndex)){
                        sortedFilmes.push(selectedRandomFilme);
                        usedIndexes.add(randomIndex)
                    }
                }
                setDisplayedFilmes(sortedFilmes)
            }else{
                console.log("Passei aqui!")
                console.log(filmes)
                setDisplayedFilmes(filmes)
            }
            
        }
       
    },[filmes])
    
   
    console.log(displayedFilmes)
    
    return(
        <>
            <section className={styles.intro}>
                <h1>Transformando Filmes em Experiências Personalizadas</h1>
                <h2>Reserve Seu Assento e Viva a Magia do Cinema!</h2>
            </section>
            <section className={styles.ticket}>
                <Carousel/>
                {/* <Slider /> */}
            </section>
            <section>
                <h1 className={styles.moviesShowTitle}>Em cartaz</h1>
                <div className={styles.moviesContainer}>
                    
                    {displayedFilmes.map( filme => {
                        return(
                            <HomeMovieCard 
                            key={filme.id} 
                            title={filme.titulo} 
                            imgUrl={filme.urlImagem} />
                        )
                    })}
                    
                </div>
                <p className={styles.seeMore}><NavLink to="/movies">Ver mais</NavLink></p>
            </section>
            
        </>
    )
}