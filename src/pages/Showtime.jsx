import { useContext, useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { useParams } from "react-router-dom";
import { ApplicationContext } from "../contexts/ApplicationContextProvider";
import  styles  from "./showtime.module.css";
import { Session } from "../components/Session";
export function Showtime(){
    

    const {filmes, setFilmes} = useContext(ApplicationContext)
    const {movieId} = useParams();
    const [foundMovie, setFoundMovie] = useState(null);
    const assetsFolder="/src/assets/";
    const [rating, setRating] = useState();
    const [list2D, setList2D] = useState([])
    const [list3D, setList3D] = useState([])
    const [listImax, setListImax] = useState([])

    useEffect(()=>{
        //Verifica se todos os dados já foram requisitados
        if(filmes.length>0){
            const findMovie = filmes.find(filme => filme.idFilme === movieId)
            setFoundMovie(findMovie)

            
        }
        
    },[filmes])
    
    const [sessoes, setSessoes] = useState([])
    useEffect(()=>{
        const basePath = "http://localhost:3000"
        const url = basePath + "/sessoes/" + movieId
        const config = {
            method:"GET"
        }
        fetch(url, config)
        .then(response => response.json())
            .then(response => {
                setSessoes(response);
                if(sessoes){
                    const sessions2D = sessoes.filter(sessao => sessao.tipo === 0)
                    setList2D(sessions2D)
                    const sessions3D = sessoes.filter(sessao => sessao.tipo === 1 )
                    setList3D(sessions3D)
                    const sessionsImax = sessoes.filter(sessao => sessao.tipo === 2 )
                    setListImax(sessionsImax)
                }
            })
    }, [sessoes])
   
    
    const getRatingImg = (rating)=>{
        const basePath = "/src/assets/"
        var ratingUrl = basePath

        if(rating === 0){
            ratingUrl += "livre.svg"
        }else
        if(rating === 1){
            ratingUrl += "10.svg"
        }else
        if(rating === 2){
            ratingUrl += "12.svg"
        }else 
        if(rating === 3){
            ratingUrl += "14.svg"
        }else
        if(rating === 4){
            ratingUrl += "16.svg"
        }else
        if(rating === 5){
            ratingUrl += "18.svg"
        }
        return ratingUrl
        
    }
   
  
  
    

       

    return(
        <div>
            <div className={styles.sessionBanner}>
                <div>
                    {foundMovie && <img src={foundMovie.urlImagem}/>}
                    <div>
                        <div className={styles.movieDescription}>
                            <div>
                                {foundMovie && <h2>{foundMovie.titulo}</h2>}
                                {foundMovie && <img className={styles.rating} src={getRatingImg(foundMovie.classificacao)}/>}
                            </div>
                            {foundMovie && <p>{foundMovie.genero}</p>}
                            {foundMovie && <p>{foundMovie.sinopse}</p>}
                        </div>
                        <div className={styles.filtersWrapper}>
                            <select id="cities">
                                <option value="">Cidade</option>
                                <option value="rj">Rio de Janeiro</option>
                                <option value="sg">São Gonçalo</option>
                                <option value="nt">Niterói</option>
                            </select>
                            <select id="neighborhood">
                            <option value="">Bairro</option>
                                <option value="barra">Barra da Tijuca - RJ</option>
                                <option value="copa">Copacabana - RJ</option>
                                <option value="centro">Centro - Niteroi</option>
                                <option value="boav">Boa vista - São Gonçalo</option>
                            </select>
                        </div>
                </div>   
                
            </div>

            </div>

            {list2D && list2D.length > 0 && <Session type={list2D[0].tipo} availableSessions={list2D}/>}
            {list3D && list3D.length > 0 && <Session type={list3D[0].tipo} availableSessions={list3D}/>}
            {listImax && listImax.length > 0 && <Session type={listImax[0].tipo} availableSessions={listImax}/>}
        </div>
        
    )
}