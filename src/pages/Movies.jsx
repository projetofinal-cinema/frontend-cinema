import { useContext, useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { ApplicationContext } from "../contexts/ApplicationContextProvider";
import { MovieCard } from "../components/MovieCard";
import styles from "./movies.module.css";


export function Movies(){
    const {filmes, setFilmes} = useContext(ApplicationContext)
    const [searchTerm, setSearchTerm] = useState('')
    const [listaFilmes, setListaFilmes] = useState([])
    useEffect(()=>{
        //Verifica se todos os dados já foram requisitados e coloca a lista completa em listaFilmes
        if(filmes.length>0){
           setListaFilmes(filmes)
        }
        
    },[filmes])
   
    const [selectedGenre, setSelectedGenre] = useState('')
    const handleGenreChange = (e)=>{
        setSelectedGenre(e.target.value)

    }

    const [selectedRating, setSelectedRating] = useState('');
        const handleRatingChange = (e)=>{
            setSelectedRating(e.target.value)
            
    }

    const filteredMovies = listaFilmes.filter(movie =>{
        
        const searchByTitle = movie.titulo.toLowerCase().includes(searchTerm.toLowerCase());
        const filterByGenre = !selectedGenre || movie.genero.includes(selectedGenre);
        console.log()
        const filterByRating = !selectedRating || movie.classificacao === parseInt(selectedRating) 
      
        return searchByTitle && filterByGenre && filterByRating;
    } );
    
    return(
        <div>
            <div className={styles.banner}>
                <div className={styles.searchWrapper}>
                    <input 
                        className={styles.searchBar}
                        type="text"
                        placeholder="Pesquisar filmes"
                        value={searchTerm}
                        onChange={e => setSearchTerm(e.target.value)}
                    />
                    <button><img src="/src/assets/lupa.svg"/></button>
                </div>
                <div className={styles.filtersWrapper}>
                    <select id="genres" onChange={handleGenreChange} >
                        <option value="">Gênero</option>
                        <option value="Ação">Ação</option>
                        <option value="Aventura">Aventura</option>
                        <option value="Comédia">Comédia</option>
                        <option value="Dança">Dança</option>
                        <option value="Documentario">Documentario</option>
                        <option value="Espionagem">Espionagem</option>
                        <option value="Faroeste">Faroeste</option>
                        <option value="Fantasia">Fantasia</option>
                        <option value="Ficção científica">Ficção Científica</option>
                        <option value="Guerra">Guerra</option>
                        <option value="Infantil">Infantil</option>
                        <option value="Mistério">Mistério</option>
                        <option value="Musical">Musical</option>
                        <option value="Policial">Policial</option>
                        <option value="Romance">Romance</option>
                        <option value="Terror">Terror</option>
                        <option value="Thriller">Thriller</option>
                    </select>
                    <select id="rating" onChange={handleRatingChange} value={selectedRating}>
                        <option value="">Classificação</option>
                        <option value="0">Livre</option>
                        <option value="1">10 anos</option>
                        <option value="2">12 anos</option>
                        <option value="3">14 anos</option>
                        <option value="4">16 anos</option>
                        <option value="5">18 anos</option>
                    </select>
                </div>
            </div>
            
            <div className={styles.moviesContainer}>
                <h1>Filmes</h1>
                <div>
                    {filteredMovies.map(filme=>{
                        return(
                        <MovieCard key={filme.idFilme}
                        id={filme.idFilme}
                        title={filme.titulo}
                        imgUrl={filme.urlImagem}
                        sinopsys={filme.sinopse}
                        genre={filme.genero}
                        rating={filme.classificacao}
                        director={filme.diretor}/>
                        )
                        
                    })}
                </div>
            </div>
        </div>
        
    )
}