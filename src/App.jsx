import { useState } from 'react'
import { BrowserRouter } from 'react-router-dom'
import { Router } from './Router'
import { ApplicationContextProvider } from './contexts/ApplicationContextProvider'
import './global.css'
function App() {
  const [count, setCount] = useState(0)

  return (
    <ApplicationContextProvider>
      <BrowserRouter>
        <Router/>
      </BrowserRouter>
    </ApplicationContextProvider>
    
  )
}

export default App
